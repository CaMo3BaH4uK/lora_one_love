#include "GyverButton.h"

class Button{
	private:
		GButton buttonSensor;
		bool buttonState = false;

	public:
		Button(int pin){
			buttonSensor = GButton(pin);
			pinMode(pin, INPUT_PULLUP);
		}

		void update_button_state(){
			buttonSensor.tick();
			if(buttonSensor.isSingle())
				buttonState = !buttonState;
		}

		bool get_state(){
			return buttonState;
		}
};
