#include <U8glib.h>

#define distance 15
#define center_y 30
#define radius 5
#define anim_step 5
#define delay_anim_time 50

U8GLIB_SSD1306_128X64 u8g(U8G_I2C_OPT_DEV_0|U8G_I2C_OPT_NO_ACK|U8G_I2C_OPT_FAST);

uint32_t _anim_pos = 0, last_anim_time = 0;

void print_data(bool _state, uint32_t _speed, uint32_t _total){
	if(_state){
		u8g.firstPage(); 
		do{
			u8g.setColorIndex(1);

			u8g.setFont(my6x13rus);
			u8g.setPrintPos(5, 20);
			u8g.print("ВС: ");
			u8g.setFont(clock_font);
			u8g.setPrintPos(30, 20);
			u8g.print(_total);
			u8g.setFont(my6x13rus);
			u8g.setPrintPos(90, 20);
			u8g.print("Л");

			u8g.setFont(my6x13rus);
			u8g.setPrintPos(5, 50);
			u8g.print("СЧ: ");
			u8g.setFont(clock_font);
			u8g.setPrintPos(30, 50);
			u8g.print(_speed);
			u8g.setFont(my6x13rus);
			u8g.setPrintPos(90, 50);
			u8g.print("мЛ/с");
		} while( u8g.nextPage() );
	}
	else{
		u8g.firstPage(); 
		do{

		} while( u8g.nextPage() );
	}
}

