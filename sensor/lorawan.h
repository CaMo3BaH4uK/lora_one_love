#pragma once
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>

// APPEUI ключ    Little endian
static const u1_t PROGMEM APPEUI[8]={ 0xAF, 0x49, 0x01, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };
void os_getArtEui(u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// DEVEUI ключ    Little endian
static const u1_t PROGMEM DEVEUI[8]={ 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
void os_getDevEui(u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// APPKEY ключ    Big endian
static const u1_t PROGMEM APPKEY[16] = { 0xBC, 0x93, 0xA2, 0x6C, 0x26, 0x4C, 0x35, 0x40, 0x49, 0x09, 0xD7, 0x23, 0xDD, 0xBF, 0x97, 0x7D };
void os_getDevKey(u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}


class lorawan {

	private:
		String inputRequestSample = "updatewater";
		String inputRequest = "";		
		uint8_t dataArray[100] = {0};
		static osjob_t sendJob;
		bool receiveFinished = false;
		bool sendRequest = false;
		int symbolsNum = 0;
		

		bool do_send(osjob_t* j, int dataLen) {
			Serial.print("Sending: ");
			Serial.write(dataArray, dataLen);
			Serial.println(" to Vega server");
			if(LMIC.opmode & OP_TXRXPEND) {
				// Serial.println(F("OP_TXRXPEND, not sending"));
				return false;
			} else {
				LMIC_setTxData2(1, dataArray, dataLen, 0);
				// Serial.println(F("Packet queued"));
				return true;
			}
		}

	public:

		void setup() {
			// Serial.begin(9600);
			// Serial.println(F("Starting"));
			os_init();
			LMIC_reset();
			LMIC_setLinkCheckMode(1);
			LMIC_setAdrMode(1);
			LMIC_setClockError(MAX_CLOCK_ERROR * 10 / 100);
			LMIC.dn2Dr = DR_SF9;		
			/* LMIC_setupChannel(0, 864700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);
			for(int i = 1; i < 9; i++)
				LMIC_disableChannel(i); */
		}

		void loopJob() {
			os_runloop_once();
		}
			
		void serialMode() {
			while(Serial.available() > 0) {
				if(receiveFinished) {
					symbolsNum = 0;
					receiveFinished = false;
				}
				uint8_t inChar = (uint8_t)Serial.read();
				if((char)inChar == '\n') {
					receiveFinished = true;
					do_send(&sendJob, symbolsNum);
				} else {
					dataArray[symbolsNum] = inChar;
					symbolsNum++;
				}
				delay(5);
			}
		}

		bool sendString(String input) {
			receiveFinished = true;
			symbolsNum = input.length();
			for(int i = 0; i < symbolsNum; i++)
				dataArray[i] = (uint8_t)input.charAt(i);
			return do_send(&sendJob, symbolsNum);
		}

		bool sendMQTTWater(int dataInt) {
			return sendString("{'water':" + String(dataInt) + "}");
		}
		
		void processInputRequest() {
			// Serial.println("Processing");
			inputRequest = "";
			for(int i = 0; i < LMIC.dataLen + 9; i++) {
				uint8_t inChar = (uint8_t)LMIC.frame[i];
				if((inChar >= 97 && inChar <= 122) || (inChar >= 65 && inChar <= 90)) {
					if(inChar >= 65 && inChar <= 90)
						inChar += 32;
					String exportChar = String(inChar, HEX);
					// Serial.print(exportChar + " ");
					inputRequest += (char)inChar;
				}
			}
			// Serial.println(inputRequest);
			if(inputRequest.indexOf(inputRequestSample) != -1) {
				sendRequest = true;
				// Serial.println("Sending request");
			}
		}

		bool checkSendRequest() {
			return sendRequest;
		}

		void completeSendRequest() {
			sendRequest = false;
		}
};



lorawan lora;

const lmic_pinmap lmic_pins = {
	.nss = 10,
	.rxtx = LMIC_UNUSED_PIN,
	.rst = 9,
	.dio = {2, 6, 7},
};

void onEvent(ev_t ev) {
	switch(ev) {
		case EV_SCAN_TIMEOUT:
			Serial.println(F("EV_SCAN_TIMEOUT"));
			break;
		case EV_BEACON_FOUND:
			Serial.println(F("EV_BEACON_FOUND"));
			break;
		case EV_BEACON_MISSED:
			Serial.println(F("EV_BEACON_MISSED"));
			break;
		case EV_BEACON_TRACKED:
			Serial.println(F("EV_BEACON_TRACKED"));
			break;
		case EV_JOINING:
			Serial.println(F("EV_JOINING"));
			break;
		case EV_JOINED:
			Serial.println(F("EV_JOINED"));
			break;
		case EV_RFU1:
			Serial.println(F("EV_RFU1"));
			break;
		case EV_JOIN_FAILED:
			Serial.println(F("EV_JOIN_FAILED"));
			break;
		case EV_REJOIN_FAILED:
			Serial.println(F("EV_REJOIN_FAILED"));
			break;
		case EV_TXCOMPLETE:
			// Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
			if(LMIC.txrxFlags & TXRX_ACK)
				// Serial.println(F("Received ack"));
			if(LMIC.dataLen) {
				// Serial.println(F("Received"));
				// Serial.println(LMIC.dataLen);
				// Serial.print(F(" bytes of payload"));
				lora.processInputRequest();	
			}
			break;
		case EV_LOST_TSYNC:
			Serial.println(F("EV_LOST_TSYNC"));
			break;
		case EV_RESET:
			Serial.println(F("EV_RESET"));
			break;
		case EV_RXCOMPLETE:
			if(LMIC.dataLen) {
				// Serial.println(F("Received"));
				// Serial.println(LMIC.dataLen);
				// Serial.print(F(" bytes of payload"));
				lora.processInputRequest();			
			}
			Serial.println(F("EV_RXCOMPLETE"));
			break;
		case EV_LINK_DEAD:
			Serial.println(F("EV_LINK_DEAD"));
			break;
		case EV_LINK_ALIVE:
			Serial.println(F("EV_LINK_ALIVE"));
			break;
		default:
			Serial.println(F("Unknown event"));
			break;
	}
}
