
class Potentiometer{
private:
	int potentiometerPin;
public:
	Potentiometer(int potentiometerPin){
		this->potentiometerPin = potentiometerPin;
	}

	float get_value(){
		return analogRead(potentiometerPin) / 19.8;
	}
};

