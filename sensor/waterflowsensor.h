
class WaterFlowSensor{
	private:

		float calibrationFactor;
		volatile uint32_t pulseCount;  
		float flowRate;
		uint32_t flowMilliLitres, totalMilliLitres;

		uint32_t timer = 0;

	public:

		WaterFlowSensor() {
			pulseCount        = 0;
			flowRate          = 0.0;
			flowMilliLitres   = 0;
			totalMilliLitres  = 0;
		}

		void change_constant(float in){
			calibrationFactor = in;
		} 

		uint32_t get_speed(){
			return flowMilliLitres;
		}

		uint32_t get_total(){
			return totalMilliLitres / 1000;
		}

		void update(){
			pulseCount *= 4;
			flowRate = ((1000.0 / (millis() - timer)) * pulseCount) / calibrationFactor;
			timer = millis();
			flowMilliLitres = (flowRate / 60) * 1000;
			totalMilliLitres += flowMilliLitres;
			pulseCount = 0;
		}

		void pretty_print() {
			uint32_t frac;

			Serial.print("Flow rate: ");
			Serial.print(uint32_t(flowRate));
			Serial.print(".");
			frac = (flowRate - uint32_t(flowRate)) * 10;
			Serial.print(frac, DEC);
			Serial.print("L/min");

			Serial.print("  Current Liquid Flowing: ");
			Serial.print(flowMilliLitres);
			Serial.print("mL/Sec");

			Serial.print("  Output Liquid Quantity: ");
			Serial.print(totalMilliLitres);
			Serial.println("mL");

			pulseCount = 0;
		}
		
		void nolora_print(){
			Serial.print(flowMilliLitres);
		}

		void pulseCounter() {
			pulseCount++;
		}
};

