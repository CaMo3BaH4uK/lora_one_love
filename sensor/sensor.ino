#include "waterflowsensor.h"
#include "button.h"
#include "potentiometer.h"
#include "display.h"
#include "lorawan.h"
#include <avr/wdt.h>

#define WATER_FLOW_SENSOR_PIN 1
#define BUTTON_PIN 4
#define POTENTIOMETER_PIN A0
#define VCC_PIN 5

WaterFlowSensor waterFlowSensor;
Button buttonSensor(BUTTON_PIN);
Potentiometer potentiometerSensor(POTENTIOMETER_PIN);

bool flag = false;

uint32_t sensorsTimer = 0, loraTimer = -20001;

void setup() {
	Serial.begin(9600);
	wdt_enable(WDTO_8S);

	lora.setup();

	pinMode(VCC_PIN, INPUT);
	digitalWrite(VCC_PIN, HIGH);

	pinMode(WATER_FLOW_SENSOR_PIN, INPUT);
	digitalWrite(WATER_FLOW_SENSOR_PIN, HIGH);

	attachInterrupt(WATER_FLOW_SENSOR_PIN, PulseCounter, FALLING);
}

void loop() {
	if(millis() % 1000 <= 250 && !flag){
		flag = true;
		attachInterrupt(WATER_FLOW_SENSOR_PIN, PulseCounter, FALLING);
	}
	else{
		flag = false;
		detachInterrupt(WATER_FLOW_SENSOR_PIN);
	}

	wdt_reset();
	lora.loopJob();

	buttonSensor.update_button_state();
	print_data(buttonSensor.get_state(), waterFlowSensor.get_speed(), waterFlowSensor.get_total());
	waterFlowSensor.change_constant(potentiometerSensor.get_value());

	if((millis() - sensorsTimer) > 1000) {
		sensorsTimer = millis();
		waterFlowSensor.update();
	}

	if((millis() - loraTimer) > 15000 || lora.checkSendRequest()){
		loraTimer = millis();
		if(lora.sendMQTTWater(waterFlowSensor.get_total()) && lora.checkSendRequest())
			lora.completeSendRequest();
	}
}

void PulseCounter(){
	waterFlowSensor.pulseCounter();
}

